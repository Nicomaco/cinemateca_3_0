var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');


var db = require('knex')({
  client: 'sqlite3',
  connection:{
    filename:"./pelis2_0.db"
  },
    useNullAsDefault: true
});
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(cors());
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.get('/api/pelDir', function(req,res){

});
app.get('/api/peliculas',function(req,res){
  db.select().from("pelicula")
      .then(function(data){
        res.json({pelicula:data});
      }).catch(function(error){
    console.log(error);
  });
});
app.get('/api/peliculas/:id',function(req,res){
  var id = parseInt(req.params.id);
  db.select().from("pelicula").where("ID_pelicula",id)
      .then(function(data){
        res.json(data)
      }).catch(function(error){
    console.log(error);
  });
});

//AGREGAR
app.post('/api/peliculas/',function(req,res){
    var data = req.body;
    db.insert(data).into("pelicula")
        .then(function(data){
            res.json(data)
        }).catch(function(error){
        console.log(error);
    });
});
//*************************************************************
//MODIFICAR!!
app.post('/api/peliculas/:id',function(req,res){
    var id = parseInt(req.params.id);
    var data = req.body;
    db("pelicula").update(data).where("ID_pelicula",id)
        .then(function(data){
            res.json(data);
        }).catch(function(error){
        console.log(error);
    });
});
app.delete('/api/peliculas/:id',function(req,res){
  var id = parseInt(req.params.id);
  db.delete().from("pelicula").where("ID_pelicula",id)
      .then(function(data){
        res.json(data)
      }).catch(function(error){
    console.log(error);
  });
});

app.get('/rss', function (req, res) {
    db.select().from("peliculas")
        .then(function (data) {
            console.log(data);
            var rss =
`<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0">

  <channel>
    <title>PELICULAS</title>
    <link>localhost:3000</link>
      <language>es</language>
    <description>Pagina que contiene 100 peliculas de directores destacados</description>`;

            for (var i = 0; i < data.length; i++) {
                rss += `
        <item>
          <title>${data[i].titulo}</title>
          <link>${data[i].imagen}</link>
          <description>
          <description>${data[i].imagen}</description>
          <category>"Pelicula"</category>
        </item>`;
            }

            rss += `
  </channel>
</rss>`;

            res.set('Content-Type', 'text/xml');
            res.send(rss);
        }).catch(function (error) {
        console.log(error);
    });
});
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;